import { ControllerRegistry } from './controller.registry';

export const Controller = (route: string): ClassDecorator => {
  return (target: any) => {
    ControllerRegistry.addController(target, { route });
  };
};
