import { model, Schema } from 'mongoose';

export const GogoSchema = new Schema(
  {
    name: String,
    color: String
  },
  { versionKey: false }
);

GogoSchema.set('toJSON', {
  transform(_: any, ret: any) {
    ret.id = ret._id;
    delete ret._id;
  }
});

export const Gogo = model('Gogo', GogoSchema);
