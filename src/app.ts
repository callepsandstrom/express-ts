import bodyParser from 'body-parser';
import cors from 'cors';
import express, { Express, Request, Response } from 'express';
import mongoose from 'mongoose';
import { environment } from './environment';
import { GogosController } from './gogos';
import { errorMiddleware, registerControllers } from './http';
import { PokemonsController } from './pokemons';

export class App {

  private app: Express;

  constructor() {
    this.app = express();
  }

  create(): App {
    this.useBodyParser();
    this.useCors();
    this.useMongoose();
    this.useDefaultRoute();
    this.useControllers();
    this.useErrorMiddleware();

    return this;
  }

  start(): void {
    this.app.listen(environment.port, () => {
      console.log(`Listening on port ${environment.port} 🚀`);
    });
  }

  private useBodyParser(): void {
    this.app.use(bodyParser.json({ limit: '50mb' }));
    this.app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
  }

  private useCors(): void {
    this.app.use(cors());
  }

  private useMongoose(): void {
    mongoose.Promise = global.Promise;
    mongoose.connect(environment.mongoDbUrl, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
  }

  private useDefaultRoute(): void {
    this.app.route('/').get((_: Request, res: Response) => res.status(200).send('Welcome 🎉'));
  }

  private useControllers(): void {
    registerControllers(this.app, [GogosController, PokemonsController]);
  }

  private useErrorMiddleware(): void {
    this.app.use(errorMiddleware);
  }
}
