import { Type } from './type';

export class Injector {
  static resolve<T>(target: Type<any>): T {
    const tokens = Reflect.getMetadata('design:paramtypes', target) as Type<any>[] || [];
    const injections = tokens.map(token => Injector.resolve<Type<any>>(token));

    return new target(...injections);
  }
}
