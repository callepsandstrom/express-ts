import { EndpointRegistry } from './endpoint.registry';

export const Get = (route: string = '') => {
  return (target: any, propertyKey: string): void => EndpointRegistry.addEndpoint(target.constructor, {
    requestMethod: 'get',
    route,
    methodName: propertyKey
  });
};

export const Delete = (route: string = '') => {
  return (target: any, propertyKey: string): void => EndpointRegistry.addEndpoint(target.constructor, {
    requestMethod: 'delete',
    route,
    methodName: propertyKey
  });
};

export const Options = (route: string = '') => {
  return (target: any, propertyKey: string): void => EndpointRegistry.addEndpoint(target.constructor, {
    requestMethod: 'options',
    route,
    methodName: propertyKey
  });
};

export const Post = (route: string = '') => {
  return (target: any, propertyKey: string): void => EndpointRegistry.addEndpoint(target.constructor, {
    requestMethod: 'post',
    route,
    methodName: propertyKey
  });
};

export const Put = (route: string = '') => {
  return (target: any, propertyKey: string): void => EndpointRegistry.addEndpoint(target.constructor, {
    requestMethod: 'put',
    route,
    methodName: propertyKey
  });
};
