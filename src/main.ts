import 'reflect-metadata';
import { App } from './app';

new App().create().start();
