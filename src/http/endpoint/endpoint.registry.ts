import { EndpointDefinition } from './endpoint.definition';

export class EndpointRegistry {
  static addEndpoint(target: any, endpoint: EndpointDefinition): void {
    Reflect.defineMetadata('endpoints', [...this.getEndpoints(target), endpoint], target);
  }

  static getEndpoints(target: any): EndpointDefinition[] {
    return Reflect.getMetadata('endpoints', target) || [];
  }
}
