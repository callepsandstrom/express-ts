import { ControllerDefinition } from './controller.definition';

export class ControllerRegistry {
  static addController(target: any, controller: ControllerDefinition): void {
    Reflect.defineMetadata('controller', controller, target);
  }

  static getController(target: any): ControllerDefinition {
    return Reflect.getMetadata('controller', target);
  }
}
