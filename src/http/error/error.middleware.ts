import { NextFunction, Request, Response } from 'express';
import { NotFoundError } from './not-found.error';

export const errorMiddleware = (error: Error | NotFoundError, _: Request, response: Response, __: NextFunction) => {
  if (error instanceof NotFoundError) {
    response.status(404).send();
  } else {
    response.status(500).send(error.message);
  }
};
