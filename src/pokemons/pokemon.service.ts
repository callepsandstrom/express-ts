import { Document } from 'mongoose';
import { NotFoundError } from '../http';
import { Pokemon } from './pokemon.model';

export class PokemonService {

  createPokemon(pokemon: Document): Promise<Document> {
    return new Promise((resolve, reject) => {
      pokemon.save((error: Error, document: Document) => !error ? resolve(document) : reject(error));
    });
  }

  getPokemons(): Promise<Document[]> {
    return new Promise((resolve, reject) => {
      Pokemon.find({}, (error: Error, documents: Document[]) => !error ? resolve(documents) : reject(error));
    });
  }

  getPokemon(id: string): Promise<Document> {
    return new Promise((resolve, reject) => {
      Pokemon.findById(id, (error: Error, document: Document | null) => {
        if (!document) {
          return reject(new NotFoundError());
        }
        if (!!error) {
          return reject(error);
        }
        return resolve(document);
      });
    });
  }

  deletePokemon(id: string): Promise<void> {
    return new Promise((resolve, reject) => {
      Pokemon.findByIdAndDelete(id, (error: Error, document: Document | null) => {
        if (!document) {
          return reject(new NotFoundError());
        }
        if (!!error) {
          return reject(error);
        }
        return resolve();
      });
    });
  }

  updatePokemon(id: string, pokemon: any): Promise<void> {
    return new Promise((resolve, reject) => {
      Pokemon.findByIdAndUpdate(id, pokemon, (error: Error, document: Document | null) => {
        if (!document) {
          return reject(new NotFoundError());
        }
        if (!!error) {
          return reject(error);
        }
        return resolve();
      });
    });
  }

}
