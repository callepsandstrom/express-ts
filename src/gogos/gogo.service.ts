import { Document } from 'mongoose';
import { NotFoundError } from '../http';
import { Gogo } from './gogo.model';

export class GogoService {

  createGogo(gogo: Document): Promise<Document> {
    return new Promise((resolve, reject) => {
      gogo.save((error: Error, document: Document) => !error ? resolve(document) : reject(error));
    });
  }

  getGogos(): Promise<Document[]> {
    return new Promise((resolve, reject) => {
      Gogo.find({}, (error: Error, documents: Document[]) => !error ? resolve(documents) : reject(error));
    });
  }

  getGogo(id: string): Promise<Document> {
    return new Promise((resolve, reject) => {
      Gogo.findById(id, (error: Error, document: Document | null) => {
        if (!document) {
          return reject(new NotFoundError());
        }
        if (!!error) {
          return reject(error);
        }
        return resolve(document);
      });
    });
  }

  deleteGogo(id: string): Promise<void> {
    return new Promise((resolve, reject) => {
      Gogo.findByIdAndDelete(id, (error: Error, document: Document | null) => {
        if (!document) {
          return reject(new NotFoundError());
        }
        if (!!error) {
          return reject(error);
        }
        return resolve();
      });
    });
  }

  updateGogo(id: string, gogo: any): Promise<void> {
    return new Promise((resolve, reject) => {
      Gogo.findByIdAndUpdate(id, gogo, (error: Error, document: Document | null) => {
        if (!document) {
          return reject(new NotFoundError());
        }
        if (!!error) {
          return reject(error);
        }
        return resolve();
      });
    });
  }

}
