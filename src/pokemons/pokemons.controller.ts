import { Request, Response } from 'express';
import { Pokemon } from '.';
import { Controller, Delete, Get, Post, Put } from '../http';
import { PokemonService } from './pokemon.service';

@Controller('/pokemons')
export class PokemonsController {

  constructor(private pokemonService: PokemonService) { }

  @Post()
  async createPokemon(request: Request, response: Response) {
    const pokemon = await this.pokemonService.createPokemon(new Pokemon(request.body));
    response.status(201).send(pokemon);
  }

  @Delete('/:id')
  async deletePokemon(request: Request, response: Response) {
    await this.pokemonService.deletePokemon(request.params.id);
    response.send();
  }

  @Get('/:id')
  async getPokemon(request: Request, response: Response) {
    const pokemon = await this.pokemonService.getPokemon(request.params.id);
    response.send(pokemon);
  }

  @Get()
  async getPokemons(_: Request, response: Response) {
    const pokemons = await this.pokemonService.getPokemons();
    response.send(pokemons);
  }

  @Put('/:id')
  async updatePokemon(request: Request, response: Response) {
    await this.pokemonService.updatePokemon(request.params.id, request.body);
    response.status(204).send();
  }
}
