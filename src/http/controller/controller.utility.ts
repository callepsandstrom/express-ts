import { Express, Request, Response } from 'express';
import { ControllerRegistry, EndpointDefinition, EndpointRegistry } from '..';
import { Injector } from '../../core';

export const registerControllers = (app: Express, targets: any[]) => targets.map(x => mapController(app, x));

const mapController = (app: Express, target: any): void => {
  const instance = Injector.resolve(target);
  const controller = ControllerRegistry.getController(target);
  const endpoints = EndpointRegistry.getEndpoints(target);

  endpoints.map(x => mapEndpoint(app, x, instance, controller.route));
};

const mapEndpoint = (app: Express, endpoint: EndpointDefinition, instance: any, route: string): void => {
  app[endpoint.requestMethod](route + endpoint.route, (req: Request, res: Response, next: any) => Promise
    .resolve(instance[endpoint.methodName](req, res, next))
    .catch(next));
};
