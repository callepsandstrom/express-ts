import { Request, Response } from 'express';
import { Gogo } from '.';
import { Controller, Delete, Get, Post, Put } from '../http';
import { GogoService } from './gogo.service';

@Controller('/gogos')
export class GogosController {

  constructor(private gogoService: GogoService) { }

  @Post()
  async createGogo(request: Request, response: Response) {
    const gogo = await this.gogoService.createGogo(new Gogo(request.body));
    response.status(201).send(gogo);
  }

  @Delete('/:id')
  async deleteGogo(request: Request, response: Response) {
    await this.gogoService.deleteGogo(request.params.id);
    response.send();
  }

  @Get('/:id')
  async getGogo(request: Request, response: Response) {
    const gogo = await this.gogoService.getGogo(request.params.id);
    response.send(gogo);
  }

  @Get()
  async getGogos(_: Request, response: Response) {
    const gogos = await this.gogoService.getGogos();
    response.send(gogos);
  }

  @Put('/:id')
  async updateGogo(request: Request, response: Response) {
    await this.gogoService.updateGogo(request.params.id, request.body);
    response.status(204).send();
  }
}
