import { model, Schema } from 'mongoose';

export const PokemonSchema = new Schema(
  {
    name: String,
    gender: String,
    type: String,
    height: Number,
    weight: Number,
    photo: String
  },
  { versionKey: false }
);

PokemonSchema.set('toJSON', {
  transform: (_: any, ret: any, options: any) => {
    ret.id = ret._id;
    delete ret._id;
  }
});

export const Pokemon = model('Pokemon', PokemonSchema);
